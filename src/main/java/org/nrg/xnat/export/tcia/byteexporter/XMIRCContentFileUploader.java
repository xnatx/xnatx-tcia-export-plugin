package org.nrg.xnat.export.tcia.byteexporter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;
import org.nrg.config.entities.Configuration;
import org.nrg.framework.constants.Scope;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.export.annotation.ByteExportHandler;
import org.nrg.xnat.export.event.ExportFileTransportEvent;
import org.nrg.xnat.export.event.publisher.ExportEventPublisher;
import org.nrg.xnat.export.exception.ExportSettingNotFoundException;
import org.nrg.xnat.export.exception.FailedToTransformException;
import org.nrg.xnat.export.exception.TransformerNotFoundException;
import org.nrg.xnat.export.interfaces.ByteExporterI;
import org.nrg.xnat.export.interfaces.ExportManagerI;
import org.nrg.xnat.export.interfaces.TransformerI;
import org.nrg.xnat.export.tcia.utils.TCIAConstants;
import org.nrg.xnat.export.transformers.TransformerHelper;
import org.nrg.xnat.export.transporters.http.HTTPResponseHolder;
import org.nrg.xnat.export.utils.ExportConstants;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Mohana Ramaratnam
 *
 */
@Component
@ByteExportHandler(handler = "TCIA_XMIRC_BYTE_EXPORTER")
@Slf4j
public class XMIRCContentFileUploader implements ByteExporterI {
    private HttpURLConnection httpConn;
    private OutputStream outputStream;
    private final String DEFAULT_CONTENT_TYPE = "application/x-mirc";
    String requestURL = null;
    UserI authorizedBy = null;
    String xnatProjectId = null;
    String xnatEventTrackingId = null;
    String destination_authorization_username = null;
    String destination_authorization_password = null;

    public void setup(Map<String, Object> setupParams) throws Exception {
    	String destinationUrl = (String)setupParams.get(ExportConstants.URL_PROP_NAME);
    	String destinationPort = (String)setupParams.get(ExportConstants.URL_PROP_PORT);
    	if (destinationUrl == null) {
    		throw new ExportSettingNotFoundException("Missing setting for " + ExportConstants.URL_PROP_NAME, null);
    	}else if ( destinationUrl != null &&  destinationPort != null) {
    		setUrl(destinationUrl, destinationPort);
    		setupExportDestination();
    	}
    	authorizedBy = (UserI)setupParams.get(ExportConstants.EXPORT_USER_KEY);
    	xnatProjectId = (String)setupParams.get(ExportConstants.EXPORT_PROJECTID_KEY);
    	xnatEventTrackingId = (String)setupParams.get(ExportConstants.EXPORT_TRACKING_EVENT_ID_KEY);
        destination_authorization_username = (String)setupParams.get(ExportConstants.AUTH_USERNAME);
        destination_authorization_password = (String)setupParams.get(ExportConstants.AUTH_PASSWORD);
    }
    
    private void setUrl(String destinationUrl , String destinationPort) throws MalformedURLException{
		requestURL = destinationUrl;
		if (destinationPort != null) {
			requestURL += ":" + destinationPort;
		}
	}
    
    private void setupExportDestination()  throws IOException {
        URL url = new URL(requestURL);
        httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setUseCaches(false);
        httpConn.setDoOutput(true); // indicates POST method
        httpConn.setDoInput(true);
        httpConn.setRequestMethod("POST");
        httpConn.setRequestProperty("Content-Type",DEFAULT_CONTENT_TYPE);
        httpConn.setRequestProperty("User-Agent", "XNAT Export Agent");
		if (destination_authorization_username != null && destination_authorization_password != null) {
			String encoding = Base64.getEncoder().encodeToString((destination_authorization_username+":"+ destination_authorization_password).getBytes());
			String authHeader = "Basic " + encoding;
			httpConn.setRequestProperty("Authorization", authHeader);
			httpConn.setRequestProperty("RSNA", destination_authorization_username+":"+destination_authorization_password); //for backward compatibility
		}

        outputStream = httpConn.getOutputStream();
    }

	public HTTPResponseHolder exportToDestination(String projectRootPath, File fileToExport, final TransformerHelper transformerHelper) throws Exception {
	    try {
	    	publish("Connecting to " + requestURL);
	    	httpConn.connect();
	    	publish("Connecting established");
	    	File inFile = null;
			String transformerHandler = transformerHelper.getTransformerHandler();
			if (transformerHandler != null) {
				inFile  = transform(projectRootPath, fileToExport, transformerHelper);
			}else {
				inFile = fileToExport;
			}
			publish("Streaming file " + inFile.getAbsolutePath());
	    	long bytesSent = streamFile(inFile);
		    //TODO handle response code
		    int responseCode = httpConn.getResponseCode();
		    String message = "";
		    if (responseCode != HttpStatus.SC_OK) {
		     message = getTextOrException(httpConn.getInputStream(), Charset.forName("UTF-8"), true);
		    }
		    return new HTTPResponseHolder(responseCode, message, 1, bytesSent);
	    }finally {
	    	httpConn.disconnect();
	    	if (outputStream != null) {
	    		outputStream.close();
	    	}
	    }
	}
	
	private File transform(String projectRootPath, File inFile, final TransformerHelper transformerHelper) throws IOException, TransformerNotFoundException,FailedToTransformException {
		File outFile = null;
		String transformerHandler = transformerHelper.getTransformerHandler();
		if (transformerHandler != null ) { //needs to be transformed
			ExportManagerI exportManager = XDAT.getContextService().getBeanSafely(ExportManagerI.class);
			if (exportManager == null) {
				throw new TransformerNotFoundException("Unable to load the transformer object");
			}
			TransformerI transformer = exportManager.getTransformerByTransportHandlerAnnotation(transformerHandler);
			if (transformer == null) {
				throw new TransformerNotFoundException("Unable to find transformer by annotation" + transformerHandler);
			}
			publish("Transforming " + inFile.getName() + " using " + transformerHandler);
			Map<String, Object> transformerParams = new HashMap<String, Object>();
			String dicomAnonFilePath = (String)transformerHelper.getTransformerSettingValue(TCIAConstants.TCIA_DICOM_ANONYMIZATION_SCRIPT_PATH);
			String dicomAnonymizationConfigPath = (String)transformerHelper.getTransformerSettingValue(TCIAConstants.TCIA_DICOM_ANONYMIZATION_CONFIG_PATH);

			String lookUpFilePath = (String)transformerHelper.getTransformerSettingValue(TCIAConstants.TCIA_LOOKUP_TABLE_PATH);
    		String lookUpConfigPath = (String)transformerHelper.getTransformerSettingValue(TCIAConstants.TCIA_LOOKUP_TABLE_CONFIG_PATH);
    		if (dicomAnonFilePath != null) {
				File dicomAnonFile = new File(dicomAnonFilePath);
				transformerParams.put("SCRIPT_FILE", dicomAnonFile);
    		}else if (dicomAnonymizationConfigPath != null) {
    			//Get the Config from the Path for the project
    			Configuration anonConfig = XDAT.getConfigService().getConfig(ExportConstants.ANON_TOOL_ID, dicomAnonymizationConfigPath, Scope.Site, null);
    			transformerParams.put("SCRIPT_FILE", anonConfig.getContents());
    		}
    		if (lookUpFilePath != null) {
				File lookUpFile = new File(lookUpFilePath);
				transformerParams.put("LOOKUP_TABLE", lookUpFile);
    		}else if (lookUpConfigPath != null) {
    			//Get the Config from the Path for the project
    			Configuration lookupConfig = XDAT.getConfigService().getConfig(ExportConstants.TCIA_LOOKUP_TOOL_ID, lookUpConfigPath, Scope.Project, xnatProjectId);
    			transformerParams.put("LOOKUP_TABLE", lookupConfig.getContents());
    		}
    		transformer.init(transformerParams);
			String transformedOutPath = transformerHelper.getTransformedRootPath();
			if (transformedOutPath == null) {
				throw new FailedToTransformException("Transformed out path is null");
			}
			File transformedOutRootDir = new File(transformedOutPath);
			if (!transformedOutRootDir.exists()) {
				transformedOutRootDir.mkdirs();
			}
			File outDir = map(inFile, projectRootPath, transformedOutRootDir);
			if (!outDir.exists()) {
				outDir.mkdirs();
			}
			boolean transformed = transformer.transform(inFile, outDir);
			if (!transformed) {
				publish("Transformation Failed");
				throw new FailedToTransformException("Could not transform " + inFile.getAbsolutePath() + " to " + outDir);
			}else {
				outFile = Paths.get(outDir.getAbsolutePath(), inFile.getName()).toFile();
				publish("Transformed to " + outFile.getAbsolutePath());
			}
		}
		return outFile;
	}
	
	
    private File map( File inFile, String projectRootPath, File outDir) throws IOException {
    	String inFilePath = inFile.getAbsolutePath();
    	String outFilePath = inFilePath.replace("\\", "/").replace(projectRootPath, outDir.getAbsolutePath());
    	File outFile = new File(outFilePath);
    	return outFile.getParentFile();
    }

	 
	private long streamFile(File file) throws Exception {
	    long bytesSent = 0;
		BufferedInputStream bis = null;
	    BufferedOutputStream bos = null;
	    byte[] buffer = new byte[4096];
	    int n=0;
	    try {
	        bis = new BufferedInputStream( new FileInputStream(file) );
	        bos = new BufferedOutputStream(outputStream);
	        while ((n=bis.read(buffer,0,buffer.length)) > 0) {
	        	bos.write(buffer,0,n);
	        	bytesSent += n;
	        }
	        bos.flush();
	        return bytesSent;
	    }
	    finally {
	        bis.close();
	        bos.close();
	    }
	}
	 
	private String getTextOrException(InputStream stream, Charset charset, boolean close) throws Exception {
	    BufferedReader br = null;
	    try {
	        br = new BufferedReader( new InputStreamReader(stream, charset) );
	        StringWriter sw = new StringWriter();
	        int n;
	        char[] cbuf = new char[1024];
	        while ((n=br.read(cbuf, 0, cbuf.length)) != -1) sw.write(cbuf,0,n);
	        if (close) br.close();
	        return sw.toString();
	    }
	    catch (Exception e) {  throw(e); }
	    finally {
	    	if (br != null) br.close();
	    }
	    
	}

	private void publish(String msg) {
		ExportFileTransportEvent exportEvent = new ExportFileTransportEvent(authorizedBy, xnatProjectId, xnatEventTrackingId, msg);
		ExportEventPublisher publisher = XDAT.getContextService().getBeanSafely(ExportEventPublisher.class);
		if (publisher != null) publisher.publishEvent(exportEvent);
	}




}
