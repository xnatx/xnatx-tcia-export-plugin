package org.nrg.xnat.export.tcia.services.impl;

import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.concurrent.ExecutorService;

import org.nrg.config.exceptions.ConfigServiceException;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.export.annotation.ExportHandler;
import org.nrg.xnat.export.credentials.SiteCredentials;
import org.nrg.xnat.export.exception.ExportSettingNotFoundException;
import org.nrg.xnat.export.exception.FailedToExportException;
import org.nrg.xnat.export.interfaces.ExportAnonymizerI;
import org.nrg.xnat.export.interfaces.ExporterI;
import org.nrg.xnat.export.manifest.ExportManifest;
import org.nrg.xnat.export.manifest.TransportManifest;
import org.nrg.xnat.export.tcia.utils.TCIAConstants;
import org.nrg.xnat.export.transformers.TransformerHelper;
import org.nrg.xnat.export.transporter.ProjectTransportBuilder;
import org.nrg.xnat.export.transporters.http.DefaultHTTPExportImpl;
import org.nrg.xnat.export.utils.ExportConstants;
import org.nrg.xnat.export.utils.ExportUtils;
import org.nrg.xnat.services.archive.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Mohana Ramaratnam (mohanakannan9@gmail.com)
 *
 */


@Component
@ExportHandler(handler = "TCIA")
@Slf4j
public class TciaExporterImpl  implements ExporterI {

	@Autowired
	public TciaExporterImpl(final CatalogService catalogService, 
							final ExportAnonymizerI exportAnonymizer,
							@Qualifier("tciaServiceThreadPoolExecutorFactoryBean")
							final ThreadPoolExecutorFactoryBean tciaServiceThreadPoolExecutorFactoryBean) {
		_catalogService = catalogService;
		_exportAnonymizer = exportAnonymizer;
		_executorService = tciaServiceThreadPoolExecutorFactoryBean.getObject();
	}


	public void export(ExportManifest exportManifest, UserI user) throws FailedToExportException  { 
		TransportManifest transportManifest = dryrun(exportManifest, user);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		transportManifest.setExportStartDate(timestamp);
		transportManifest.setAuthorizedBy(user);
		String project = transportManifest.getProject().getId();
		String exportHandler = transportManifest.getExportManifest().getEndpointDefinition().getExportHandler();
		transportManifest.setExportEventId(ExportUtils.buildExportEventId(project, timestamp.toString(), exportHandler));

		try {
			String	destinationUrl = transportManifest.getExportManifest().getEndpointDefinition().getExportSettingValueForProp(ExportConstants.URL_PROP_NAME);
			String	destinationPort = transportManifest.getExportManifest().getEndpointDefinition().getExportSettingValueForProp(ExportConstants.URL_PROP_PORT);

			String transformerHandler = exportManifest.getEndpointDefinition().getExportSettingValueForProp(ExportConstants.TRANSFORMER_HANDLER);
			String dicomAnonymizationPath = exportManifest.getEndpointDefinition().getInputSettingValueForProp(TCIAConstants.TCIA_DICOM_ANONYMIZATION_SCRIPT_PATH);
			String dicomAnonymizationConfigPath = exportManifest.getEndpointDefinition().getInputSettingValueForProp(TCIAConstants.TCIA_DICOM_ANONYMIZATION_CONFIG_PATH);
	    	TransformerHelper transformerHelper = new TransformerHelper();
	    	transformerHelper.addToTransformerSetting(ExportConstants.EXPORT_USER_KEY, user);
	    	transformerHelper.addToTransformerSetting(ExportConstants.EXPORT_PROJECTID_KEY, project);
	    	transformerHelper.addToTransformerSetting(ExportConstants.EXPORT_TRACKING_EVENT_ID_KEY, transportManifest.getExportEventId());


	    	transformerHelper.setProjectRootPath(transportManifest.getProject().getArchiveRootPath());
	    	transformerHelper.setByteExporterHandler(TCIAConstants.TCIA_CTP_BYTE_EXPORTER);
	    	transformerHelper.addToTransformerSetting(ExportConstants.URL_PROP_NAME, destinationUrl);
	    	transformerHelper.addToTransformerSetting(ExportConstants.URL_PROP_PORT, destinationPort);
	    	if (transportManifest.getExportManifest().getEndpointDefinition().isCredentialsRequired()) {
				String	uName = transportManifest.getExportManifest().getEndpointDefinition().getInputSettingValueForProp(ExportConstants.AUTH_USERNAME);
				String	pWord = transportManifest.getExportManifest().getEndpointDefinition().getInputSettingValueForProp(ExportConstants.AUTH_PASSWORD);
		    	if (uName != null && pWord != null) {
		    		SiteCredentials siteCredentials = new SiteCredentials(uName, pWord);
			    	transportManifest.getExportManifest().setCredentials(siteCredentials);
			    	transformerHelper.addToTransformerSetting(ExportConstants.AUTH_USERNAME, uName);
			    	transformerHelper.addToTransformerSetting(ExportConstants.AUTH_PASSWORD, pWord);
		    	}
	    	}
	    	
		    
			if (transformerHandler != null) {
		    	final String cachePath = XDAT.getSiteConfigurationProperty("cachePath");
		    	String transformedOutPath = Paths.get(cachePath,project,"TRANSFORMED",transportManifest.getExportEventId().replace("/", "_").replace(" ", "_").replace(":", "_")).toString();
		    	transformerHelper.setTransformedRootPath(transformedOutPath);
		    	transformerHelper.setTransformerHandler(transformerHandler);
		    	if (dicomAnonymizationPath != null) {
		    		transformerHelper.addToTransformerSetting(TCIAConstants.TCIA_DICOM_ANONYMIZATION_SCRIPT_PATH, dicomAnonymizationPath);
		    	}else if (dicomAnonymizationConfigPath != null) {
		    		transformerHelper.addToTransformerSetting(TCIAConstants.TCIA_DICOM_ANONYMIZATION_CONFIG_PATH, dicomAnonymizationConfigPath);
		    	}
		    	String lookUpTablePath = exportManifest.getEndpointDefinition().getInputSettingValueForProp(TCIAConstants.TCIA_LOOKUP_TABLE_PATH);
				String lookUpTable = exportManifest.getEndpointDefinition().getInputSettingValueForProp(TCIAConstants.TCIA_LOOKUP_TABLE_CONFIG_PATH);
				if (lookUpTablePath != null) {
		    		transformerHelper.addToTransformerSetting(TCIAConstants.TCIA_LOOKUP_TABLE_PATH,lookUpTablePath);
		    	}else if (lookUpTable != null) {
		    		transformerHelper.addToTransformerSetting(TCIAConstants.TCIA_LOOKUP_TABLE_CONFIG_PATH,lookUpTable);
		    	}
				transportManifest.setTransformerHelper(transformerHelper);
		    }
		    
		}catch(ExportSettingNotFoundException | ConfigServiceException e) {
			throw new FailedToExportException(e.getMessage(), new IllegalArgumentException(e.getMessage()));
		}
		
		//Now write all these files to the export location
        try {
            _executorService.submit(new DefaultHTTPExportImpl(transportManifest));
        } catch (Exception e) {
        	log.error("Could not submit job");
        	throw new FailedToExportException("Could not enqueue export task for project " + project, new IllegalArgumentException("Could not enqueue export task for project " + project));
        }
	}
	
	

	public TransportManifest dryrun(ExportManifest exportManifest, UserI user) { 
		ProjectTransportBuilder transportProjectBuilder = new ProjectTransportBuilder(exportManifest,user);
		TransportManifest transportManifest = null;
		try {
			transportManifest = transportProjectBuilder.constructTransportManifest(); 
		}catch(Exception e) {
			log.error("Couldnot dryrun", e);
		}
		return transportManifest;
	}
	
	 private final CatalogService _catalogService;
	 private final ExportAnonymizerI _exportAnonymizer;
	 private final ExecutorService _executorService;
}
