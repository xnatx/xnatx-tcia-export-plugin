package org.nrg.xnat.export.tcia.plugin;

import java.io.IOException;

import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xnat.export.tcia.byteexporter.XMIRCContentFileUploader;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Mohana Ramaratnam
 *
 */
@XnatPlugin(value = "TCIAExporter",
name = "TCIA Export plugin",
description = "This is the XNAT 1.7 TCIA Export Plugin dicom file export",
logConfigurationFile = "META-INF/resources/tcia-export-logback.xml"
)
@ComponentScan({"org.nrg.xnat.export.tcia.services.impl"})

@Slf4j

public class TciaPlugin {
	
	    @Bean(name = "tciaServiceThreadPoolExecutorFactoryBean")
	    public ThreadPoolExecutorFactoryBean tciaServiceThreadPoolExecutorFactoryBean() {
	        ThreadPoolExecutorFactoryBean tBean = new ThreadPoolExecutorFactoryBean();
	        tBean.setCorePoolSize(5);
	        tBean.setThreadNamePrefix("export-tcia-");
	        return tBean;
	    }

	   
}
