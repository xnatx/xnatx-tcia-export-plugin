package org.nrg.xnat.export.tcia.utils;

/**
 * @author Mohana Ramaratnam
 *
 */
public class TCIAConstants {

	   public static final String TCIA_DICOM_ANONYMIZATION_SCRIPT_PATH = "dicomanonymizationscriptpath";
	   public static final String TCIA_DICOM_ANONYMIZATION_CONFIG_PATH = "dicomanonymizationconfigpath";
	   public static final String TCIA_LOOKUP_TABLE_PATH = "lookuptablefilepath";
	   public static final String TCIA_LOOKUP_TABLE_CONFIG_PATH = "lookuptableconfigpath";
	   public static final String TCIA_CTP_BYTE_EXPORTER = "TCIA_XMIRC_BYTE_EXPORTER";
	   
	 
}
