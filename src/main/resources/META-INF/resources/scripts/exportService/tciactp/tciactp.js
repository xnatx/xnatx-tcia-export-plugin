  var XNAT = getObject(XNAT || {});

  (function(factory){
      if (typeof define === 'function' && define.amd) {
          define(factory);
      }
      else if (typeof exports === 'object') {
          module.exports = factory();
      }
      else {
          return factory();
      }
  }(function(){

        var rootUrl = XNAT.url.rootUrl,
        csrfUrl = XNAT.url.csrfUrl;

    	XNAT.app = getObject(XNAT.app || {});

        XNAT.app.tciactpExporter = getObject(XNAT.app.tciactpExporter || {});

        XNAT.app.tciactpExporter = tciactpExporter = getObject(XNAT.app.tciactpExporter || {});

 		var anonScripts = [];
 		var lookUpScripts = [];
 		var projectId = XNAT.data.context.projectID,
 		endpointDefinition;

	   function errorHandler(e, title, closeAll){
        console.log(e);
        title = (title) ? 'Error Found: '+ title : 'Error';
        closeAll = (closeAll === undefined) ? true : closeAll;
        var errormsg = (e.statusText) ? '<p><strong>Error ' + e.status + ': '+ e.statusText+'</strong></p><p>' + e.responseText + '</p>' : e;
        XNAT.dialog.open({
            width: 450,
            title: title,
            content: errormsg,
            buttons: [
                {
                    label: 'OK',
                    isDefault: true,
                    close: true,
                    action: function(){
                        if (closeAll) {
                            xmodal.closeAll();

                        }
                    }
                }
            ]
        });
      }


		function siteAnonScriptsUrl(appended, cacheParam){
			appended = appended ? '/' + appended : '';
			return restUrl('/data/config/export-service-anon' + appended, {format:'json'}, cacheParam || false);
		}

		function projectLookupScriptsUrl(appended, cacheParam){
			appended = appended ? '/' + appended : '';
			return restUrl('/data/projects/'+projectId +'/config/export-service-tcia-lookup' + appended, {format:'json'}, cacheParam || false);
		}

		function projectExportEndpointUrl(appended,  cacheParam){
		        appended = appended ? '/' + appended : '';
		        return restUrl('/xapi/export/projects'   +   appended, {}, cacheParam || false);
		}

		function projectVerifyEndpointUrl(appended,  cacheParam){
		        appended = appended ? '?' + appended : '';
		        return restUrl('/xapi/export/endpoint/verify'   +   appended, {}, cacheParam || false);
		}



 		function setSiteWideAnonScripts() {
			XNAT.xhr.get({
						url: siteAnonScriptsUrl(null),
						dataType: 'json',
						async: false,
						success: function(data){
							data.ResultSet.Result.forEach(function(item){
								var enabled = item.status === "enabled";
								if (enabled) {
									anonScripts.push(item.path);
								}
							});
						}
			});
		};

 		function setProjectLookupScripts() {
			XNAT.xhr.get({
						url: projectLookupScriptsUrl(null),
						dataType: 'json',
						async: false,
						success: function(data){
							data.ResultSet.Result.forEach(function(item){
								var enabled = item.status === "enabled";
								if (enabled) {
									lookUpScripts.push(item.path);
								}
							});
						},
						fail: function(e) {console.log("Could not get the lookup scripts" );}
			});
		};


		function loadScripts() {
			setSiteWideAnonScripts();
			setProjectLookupScripts();
		}

		function getSiteWideAnonSelectionElement() {
			return XNAT.ui.panel.select.single({
						id: 'siteAnon',
						name: 'siteAnon',
						label: 'Site Anon Scripts',
						//data: dataProps,
						//attr: attr,
						//className: classes.join(' '),
						description: 'Site wide anon scripts',
						options: anonScripts
			}).element;
		}

		function getProjectLookupSelectionElement() {
			return XNAT.ui.panel.select.single({
						id: 'projectLookup',
						name: 'projectLookup',
						label: 'Project Lookup Map',
						//data: dataProps,
						//attr: attr,
						//className: classes.join(' '),
						description: 'Project Lookup Map',
						options: lookUpScripts
			}).element;
		}



		function getProjectDataElement() {
			var attr ={};
			attr['checked'] = 'checked';
		    return spawn('div', [
		            spawn('label.element-label', '<b>Data to export:</b>'),
		            spawn('div.element-wrapper', [
		                spawn('label', [
		                    spawn('input', { type: 'checkbox', disabled: 'disabled', id:'projectDataToExport', name: 'projectDataToExport', value: '', attr: attr }),
		                    'All Imaging sessions with DICOM scans'
		                ])
		            ])
		        ]);
		}

		function verifyButton() {
		   return spawn('button.btn.sm.verify', {
							onclick: function(e){
								e.preventDefault();
								var destinationUrl = getDestinationServerUrl();
								var u = document.getElementById('username').value;
								var p = document.getElementById('password').value;

								var encodedParams = projectVerifyEndpointUrl('destination='+encodeURIComponent(destinationUrl)+'&username='+u+'&password='+encodeURIComponent(p));
								XNAT.xhr.post({
									url: encodedParams,
									contentType: 'application/json',
									success: function(){
											 XNAT.dialog.open({
														width: 450,
														title: 'TCIA-CTP Password verified',
														content: 'Credential supplied for <b> ' + destinationUrl + '</b> is correct',
														buttons: [
															{
																label: 'OK',
																isDefault: true,
																close: true,
																action: function(){
																		xmodal.closeAll();
																}
															}
														]
													});
									},
									fail: function(e) {
											 XNAT.dialog.open({
														width: 450,
														title: 'ERROR - TCIA-CTP Password verification failed ',
														content: 'Credential supplied for <b> ' + destinationUrl + '</b> is incorrect. Please try again.',
														buttons: [
															{
																label: 'OK',
																isDefault: true,
																close: true,
																action: function(){
																		xmodal.closeAll();
																}
															}
														]
													});
									}
								});
							}
				}, 'Verify');
		}

		function getInput(inputname) {
			var inputs = endpointDefinition.inputs;
			var iFound = null;
			if (inputs) {
				for (var i=0; i< inputs.length;i++) {
					if (inputs[i].name===inputname) {
						iFound = inputs[i];
						break;
					}
				}
			}
			return iFound;
		}

		function getExportSettings(eSettingName) {
					var exportSettings = endpointDefinition.export_settings;
					var iFound = null;
					for (var i=0; i< exportSettings.length;i++) {
						if (exportSettings[i].name===eSettingName) {
							iFound = exportSettings[i];
							break;
						}
					}
					return iFound;
		}

		function getDestinationUrl() {
					var url = getExportSettings('url');
					if (url === null) {
						errorHandler('Missing export setting',"Expected export setting url not found",null);
					}

					var port = getExportSettings('port');
					var destinationUrl = url.value;
					if ( port !== null) {
					   destinationUrl += ":" + port.value;
					}
					return destinationUrl;
		}

		function getDestinationServerUrl() {
					var url = getExportSettings('url');
					if (url === null) {
						errorHandler('Missing export setting',"Expected export setting url not found",null);
					}

					var port = getExportSettings('serverport');
					var destinationUrl = url.value;
					if ( port !== null) {
					   destinationUrl += ":" + port.value;
					}
					return destinationUrl;
		}

		function doExport() {
			var siteAnonOpts = document.getElementById("siteAnon");
			var projectLookupOpts = document.getElementById("projectLookup");
			var siteAnonVal = '';
			var projectLookupVal = '';
			if (typeof siteAnonOpts != 'undefined' && siteAnonOpts != null) {
				siteAnonVal = siteAnonOpts.options[siteAnonOpts.selectedIndex].value;
			}
			if (typeof projectLookupOpts != 'undefined' && projectLookupOpts != null) {
				projectLookupVal = projectLookupOpts.options[projectLookupOpts.selectedIndex].value;
			}
			//Construct the JSON with the above options
			var inputs = endpointDefinition.inputs;
			if (inputs) {
				inputs.splice(0, inputs.length);
			} else {
				inputs = [];
			}
			if (siteAnonVal != "") {
				inputs.push({
      				"name": "dicomanonymizationconfigpath",
      				"description": "Anonymization Config Path",
      				"type": "string",
      				"value": siteAnonVal,
      				"required": true
    			});
			}
			if (projectLookupVal != '') {
				inputs.push({
      				"name": "lookuptableconfigpath",
      				"description": "Lookuptable Config Path",
      				"type": "string",
      				"value": projectLookupVal,
      				"required": true
    			});
			}
			if (endpointDefinition.credentials_required) {
				//Add username and password
				var u = document.getElementById('username').value;
				var p = document.getElementById('password').value;
				inputs.push({
      				"name": "username",
      				"description": "Username",
      				"type": "string",
      				"value": u,
      				"required": true
    			});
				inputs.push({
      				"name": "password",
      				"description": "Password",
      				"type": "string",
      				"value": p,
      				"required": true
    			});

			}
			XNAT.xhr.post({
				url: projectExportEndpointUrl(projectId),
				contentType: 'application/json',
                data: JSON.stringify(endpointDefinition),
				success: function(){
					 XNAT.dialog.open({
								width: 450,
								title: 'Export Launch Status',
								content: 'Project <b> ' + projectId + '</b> export has been queued',
								buttons: [
									{
										label: 'OK',
										isDefault: true,
										close: true,
										action: function(){
												xmodal.closeAll();
										}
									}
								]
							});
				}
			});
		}


		tciactpExporter.openLaunchDialog = function(exportEndpoint) {
       			endpointDefinition = exportEndpoint;
       			var formContent = [];
       				formContent.push(
       				     spawn('div.destination-settings'),
       				     spawn('div.credential-settings'),
       				     spawn('div.anonymization-settings'),
       				     spawn('div.data-selection-settings')
       			    );
      			var launcherContent = spawn('div.panel',[spawn('form',formContent)]);
				loadScripts();


    			XNAT.ui.dialog.open({
    	            title: 'Export to ' + exportEndpoint.label,
    	            content: launcherContent,
    	            width: 750,
    		        maxBtn: true,
    	            scroll: true,
    	            beforeShow: function(obj){
    	                xmodal.loading.close();
    	                xmodal.loading.open({title: 'Rendering Export Launcher'});
    	                var $panel = obj.$modal.find('.panel'),
    	                $destinationContainer = $panel.find('.destination-settings');
    	                $credentialContainer = $panel.find('.credential-settings');
    	                $anonymizationContainer = $panel.find('.anonymization-settings');
    	                $dataContainer = $panel.find('.data-selection-settings');
    	                var destinationUrl = getDestinationUrl();

                        $destinationContainer.append(spawn('p',[ spawn('strong', 'Destination: ' + destinationUrl  )]));
						if (exportEndpoint.credentials_required) {
							spawn('br.clear');
	                        $credentialContainer.append(spawn('p',[ spawn('strong', 'Provide destination credentials: ')]));
	                        $credentialContainer.append(spawn('p', [ spawn('span','Username: '), spawn('input', { type: 'text', id:'username', name: 'username', value: ''}), spawn('i.spacer', {
								style: {
									display: 'inline-block',
									width: '10px'
								}
							}), spawn('span','Password: '), spawn('input', { type: 'text', id:'password', name: 'password', value: ''}), verifyButton() ]));
	                        spawn('br.clear');
						}
						if (anonScripts.length > 0) {
	                        spawn('br.clear');
	                        $anonymizationContainer.append(spawn('p',[ spawn('strong', 'Select the anonymization script to apply: ')]));
                        	$anonymizationContainer.append(getSiteWideAnonSelectionElement());
							spawn('br.clear');
						}
						if (lookUpScripts.length > 0) {
							spawn('br.clear');
	                        $anonymizationContainer.append(spawn('p',[ spawn('strong', 'Select the lookup map to apply: ')]));
							$anonymizationContainer.append(getProjectLookupSelectionElement());
							spawn('br.clear');
						}
						spawn('br.clear');
						$dataContainer.append(getProjectDataElement());
						spawn('br.clear');
        	        },
        	        afterShow: function(obj){
						var dicomanonymizationconfigpathInputSetting = getInput('dicomanonymizationconfigpath');
						var lookuptableconfigpathInputSettting = getInput('lookuptableconfigpath');
						if (dicomanonymizationconfigpathInputSetting && lookuptableconfigpathInputSettting) {
							var dicomanonymizationconfigpathInputValue = dicomanonymizationconfigpathInputSetting.value;
							var lookuptableconfigpathInputValue = lookuptableconfigpathInputSettting.value;
							var siteAnonOpts = document.getElementById("siteAnon");
							var projectLookupOpts = document.getElementById("projectLookup");

							for (var i=0; i < anonScripts.length; i++) {
								if (anonScripts[i] === dicomanonymizationconfigpathInputValue) {
									siteAnonOpts.options[i].selected = true;
								}
							}
							for (var i=0; i < lookUpScripts.length; i++) {
								if (lookUpScripts[i] === lookuptableconfigpathInputValue) {
									projectLookupOpts.options[i].selected = true;
								}
							}
						}
        	            xmodal.loading.close();
        	        },
        	        buttons: [
                    {
                        label: "Export",
                        isDefault: true,
                        onclick: function(e){ doExport();},
                        close: true
                     },
		             {
		                label: 'Cancel',
		                isDefault: false,
	                    close: true
			         }
			         ]
            });


		};


}));

