/*
 * web: exportEndpointManagerLookup.js
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

/*!
 * Manage Export Endpoint Anonymizations
 */

console.log('exportEndpointManagerLookup.js');

var XNAT = getObject(XNAT || {});

(function(factory){
    if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    else if (typeof exports === 'object') {
        module.exports = factory();
    }
    else {
        return factory();
    }
}(function(){

    var exportEndpointManagerLookup, undefined, undef,
        rootUrl = XNAT.url.rootUrl,
        restUrl = XNAT.url.restUrl;

    XNAT.admin =
        getObject(XNAT.admin || {});

    XNAT.admin.exportEndpointManagerLookup = exportEndpointManagerLookup =
        getObject(XNAT.admin.exportEndpointManagerLookup || {});


    function spacer(width){
        return spawn('i.spacer', {
            style: {
                display: 'inline-block',
                width: width + 'px'
            }
        })
    }

   function errorHandler(e, title, closeAll){
        console.log(e);
        title = (title) ? 'Error Found: '+ title : 'Error';
        closeAll = (closeAll === undefined) ? true : closeAll;
        var errormsg = (e.statusText) ? '<p><strong>Error ' + e.status + ': '+ e.statusText+'</strong></p><p>' + e.responseText + '</p>' : e;
        XNAT.dialog.open({
            width: 450,
            title: title,
            content: errormsg,
            buttons: [
                {
                    label: 'OK',
                    isDefault: true,
                    close: true,
                    action: function(){
                        if (closeAll) {
                            xmodal.closeAll();

                        }
                    }
                }
            ]
        });
    }

     function getUrlParams(){
	        var paramObj = {};

	        // get the querystring param, redacting the '?', then convert to an array separating on '&'
	        var urlParams = window.location.search.substr(1,window.location.search.length);
	        urlParams = urlParams.split('&');

	        urlParams.forEach(function(param){
	            // iterate over every key=value pair, and add to the param object
	            param = param.split('=');
	            paramObj[param[0]] = param[1];
	        });

	        return paramObj;
	    }

	function getProjectId(){
	        if (XNAT.data.context.projectID.length > 0) return XNAT.data.context.projectID;
	        return getUrlParams().id;
    }

    function anonEndpointUrl(appended, cacheParam){
        appended = appended ? '/' + appended : '';
        var projectId = getProjectId();
        return restUrl('/data/projects/'+projectId + '/config/export-service-tcia-lookup' + appended, {format:'json'}, cacheParam || false, true);
    }



    // keep track of used labels to help prevent label conflicts
    exportEndpointManagerLookup.paths = [];

    // get the list of Export Endpoints
    exportEndpointManagerLookup.getLookups = exportEndpointManagerLookup.getAll = function(callback){
        callback = isFunction(callback) ? callback : function(){};
        return XNAT.xhr.get({
            url: anonEndpointUrl(null, true),
            success: function(data){
                exportEndpointManagerLookup.definitions = data;
                data.ResultSet.Result.forEach(function(item){
                    exportEndpointManagerLookup.paths.push(item.path);
                });
                callback.apply(this, arguments);
            },
            fail:function(e){  console.log("Failed to get export lookup");}
        });
    };


    exportEndpointManagerLookup.getAnon = exportEndpointManagerLookup.getOne = function(path, callback){
        if (!path) return null;
        callback = isFunction(callback) ? callback : function(){};
        return XNAT.xhr.get({
            url: anonEndpointUrl(path, true),
            success: callback,
            fail: function(e){errorHandler(e,"Failed to get tcia export lookup",false);}
        });
    };

    exportEndpointManagerLookup.get = function(path){
        if (!path) {
            return exportEndpointManagerLookup.getAll();
        }
        return exportEndpointManagerLookup.getOne(path);
    };

    // dialog to create/edit endpoint anonymization
   exportEndpointManagerLookup.dialog = function(endpointAnon,newCommand){
        var _source,_editor, _path;
         if (!newCommand) {
			var endpointAnonObj = endpointAnon.ResultSet.Result[0];
 	        var path = endpointAnonObj.path;

 			path = path || {};

            var dialogButtons = {
                update: {
                    label: 'Save',
                    isDefault: true,
                    action: function(){
                        var editorContent = _editor.getValue().code;

                        var url = anonEndpointUrl(path);

                        XNAT.xhr.put({
                            url: url,
                            contentType: 'text/plain',
                            data: editorContent,
                            success: function(){
                                exportEndpointManagerLookup.refreshTable();
                                xmodal.closeAll();
                                XNAT.ui.banner.top(2000, 'TCIA Export lookup script updated.', 'success');
                            },
                            fail: function(e){
                                errorHandler(e, 'Could Not Update', false);
                            }
                        });
                    }
                },
                close: { label: 'Cancel' }
            };

            _source = spawn ('textarea', endpointAnonObj.contents);
			_editor = XNAT.app.codeEditor.init(_source, {
                language: 'text'
            });

            _editor.openEditor({
                title: 'Edit Export Lookup for ' + path,
                classes: 'plugin-text',
                buttons: dialogButtons,
                height: 680,
                afterShow: function(dialog, obj){
                    obj.aceEditor.setReadOnly(false);
                    dialog.$modal.find('.body .inner').prepend(
                        spawn('div',[
                            spawn('p', 'TCIA Export Lookup Label: '+path),
                        ])
                    );
                }
            });


        } else {
            _source = spawn('textarea', '');
            _editor = XNAT.app.codeEditor.init(_source, {
	                    language: 'text'
	         });



	                _editor.openEditor({
	                    title: 'Add New TCIA Export Lookup',
	                    classes: 'plugin-text',
						afterShow: function(dialog, obj){
							dialog.$modal.find('.body .inner').prepend(
								spawn('div',  [
											spawn('label.element-label', 'Path for TCIA Lookup:'),
											spawn('div.element-wrapper', [
												spawn('label', [
													spawn('input', { type: 'text',id:'path', name: 'path', value: ''})
												])
											])
										])
							);
						},
	                    buttons: {
	                        create: {
	                            label: 'Save',
	                            isDefault: true,
	                            action: function(){
	                                var editorContent = _editor.getValue().code;
									var pathElt = $('input#path');
									var pathVal = pathElt.val();
									if (pathVal === "") {
											XNAT.dialog.open({
												width: 450,
												title: 'Error',
												content: 'Please enter value for path to save this lookup to',
												buttons: [
													{
														label: 'Close',
														isDefault: true,
														close: true
													}
												]
											});
									}
	                                var url = anonEndpointUrl(pathVal+'?unversioned=true');

	                                XNAT.xhr.put({
	                                    url: url,
	                                    contentType: 'text/plain',
	                                    data: editorContent,
	                                    success: function(obj){
	                                        exportEndpointManagerLookup.refreshTable();
	                                        xmodal.close(obj.$modal);
	                                        XNAT.ui.banner.top(2000, 'TCIA Export lookup created.', 'success');
	                                    },
	                                    fail: function(e){
	                                        errorHandler(e, 'Could Not Save', false);
	                                    }
	                                });
	                            }
	                        },
	                        close: {
	                            label: 'Cancel'
	                        }
	                    }
	                });

        }
    };

    // create table for Export Anon scipts
    exportEndpointManagerLookup.table = function(container, callback){

        // initialize the table - we'll add to it below
        var exportEndpointLookupTable = XNAT.table({
            className: 'export-endpoint-lookup xnat-table',
            style: {
                width: '100%',
                marginTop: '15px',
                marginBottom: '15px'
            }
        });

        // add table header row
        exportEndpointLookupTable.tr()
                .th({ addClass: 'left', html: '<b>Label</b>' })
                .th('<b>Enabled</b>')
                .th('<b>Actions</b>');

        // TODO: move event listeners to parent elements - events will bubble up
        // ^-- this will reduce the number of event listeners
        function enabledCheckbox(item){
            var enabled = item.status === "enabled";
            var ckbox = spawn('input.export-endpoint-lookup-enabled', {
                type: 'checkbox',
                checked: enabled,
                value: enabled,
                data: { path: item.path},
                onchange: function(){
                    // save the status when clicked
                    var checkbox = this;
                    enabled = checkbox.checked;
                    var enabledTxt =enabled?'enabled':'disabled'
                    XNAT.xhr.put({
                        url: anonEndpointUrl(item.path + '?status=' + enabledTxt),
                        success: function(){
                            var status = (enabled ? ' enabled' : ' disabled');
                            checkbox.value = enabled;
                            XNAT.ui.banner.top(1000, '<b>' + item.path + '</b> ' + status, 'success');
                            console.log(item.path + status)
                        }
                    });
                }
            });
            return spawn('div.center', [
                ['label.switchbox|title=' + item.path, [
                    ckbox,
                    ['span.switchbox-outer', [['span.switchbox-inner']]]
                ]]
            ]);
        }

        function editLink(item, text){
            return spawn('a.link|href=#!', {
                onclick: function(e){
                    e.preventDefault();
                    if (item) {
                        exportEndpointManagerLookup.getAnon(item.path, function(data){
                            exportEndpointManagerLookup.dialog(data, false);
                        });
                    }
                    else {
                        exportEndpointManager.dialog('', false);
                    }
                }
            }, [['b', text]]);
        }

        function editButton(item){
            return spawn('button.btn.sm.edit', {
                onclick: function(e){
                    e.preventDefault();
                    if (item) {
                        exportEndpointManagerLookup.getAnon(item.path, function(data){
                            exportEndpointManagerLookup.dialog(data, false);
                        });
                    }
                    else {
                        exportEndpointManagerLookup.dialog('', false);
                    }
                }
            }, 'Edit');
        }

        function deleteButton(item){
            return spawn('button.btn.sm.delete', {
                onclick: function(){
                    XNAT.dialog.confirm({
                        // height: 220,
                        title: 'Delete TCIA-CTP export lookup?',
                        scroll: false,
                        content: '' +
                        "<p>Are you sure you'd like to delete the '<b>" + item.path + "</b>' TCIA export lookup?</p>" +
                        '<p><b><i class="fa fa-exclamation-circle"></i> This action cannot be undone.</b></p>' +
                        "",
                        okLabel: "Delete",
                        okAction: function(){
                            console.log('delete label ' + item.path);
                            XNAT.xhr.delete({
                                url: anonEndpointUrl(item.path),
                                success: function(){
                                    console.log('"' + item.path + '" deleted');
                                    XNAT.ui.banner.top(1000, '<b>"' + item.path + '"</b> deleted.', 'success');
                                    refreshTable();
                                }
                            });
                        }
                    })
                }
            }, 'Delete');
        }

        exportEndpointManagerLookup.getAll().done(function(data){
            data.ResultSet.Result.forEach(function(item){
                var identifierLabel = item.path || 'exportEndpointAnonObjectLabel';
                identifierLabel += (identifierLabel === 'exportEndpointAnonObjectLabel') ? ' (Default)' : '';
                exportEndpointLookupTable.tr({ title: item.path, data: { path: item.path} })
                        .td([item.path]).addClass('path')
                        .td([enabledCheckbox(item)]).addClass('status')
                        //.td([['div.center', [editButton(item), spacer(10), deleteButton(item)]]]);
                        .td([['div.center', [editButton(item)]]]);
            });
            if (container) {
                $$(container).append(exportEndpointLookupTable.table);
            }

            if (isFunction(callback)) {
                callback(exportEndpointLookupTable.table);
            }

        });

        exportEndpointManagerLookup.$table = $(exportEndpointLookupTable.table);

        return exportEndpointLookupTable.table;
    };

    exportEndpointManagerLookup.init = function(container){
            var $manager = $$(container || 'div#proj-export-config-lookup-container');

            exportEndpointManagerLookup.$container = $manager;


			exportEndpointManagerLookup.getLookups().done(function(data){

				exportEndpointManagerLookup.definitions = data;

				$manager.append(exportEndpointManagerLookup.table());

			});

            var newEndpoint = spawn('button.new-export-endpoint-lookup.btn.btn-sm.submit', {
                html: 'Add New TCIA Lookup',
                onclick: function(){
                    exportEndpointManagerLookup.dialog(null, true);
                }
            });

            // add  'add new' buttons at the bottom
            $manager.append(spawn('div', [
                newEndpoint,
                ['div.clear.clearfix']
            ]));

            return {
                element: $manager[0],
                spawned: $manager[0],
                get: function(){
                    return $manager[0]
                }
            };

    };


   exportEndpointManagerLookup.refresh = exportEndpointManagerLookup.refreshTable = function(){
        if (typeof exportEndpointManagerLookup.$table != "undefined" ) {
		   exportEndpointManagerLookup.$table.remove();
        }
        exportEndpointManagerLookup.table(null, function(table){
            exportEndpointManagerLookup.$container.prepend(table);
        });
    };

    exportEndpointManagerLookup.init();

    return XNAT.admin.exportEndpointManagerLookup = exportEndpointManagerLookup;

}));
