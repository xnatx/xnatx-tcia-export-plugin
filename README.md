# README #

### What is this repository for? ###

* June 25, 2020

This plugin contains code to send (export) an XNAT project to a CTP endpoint via HTTP.

* Version 0.1

### How do I get set up? ###

* Needs XNAT 1.7.7 active-develop


### How to build ###

gradlew clean fatJar

